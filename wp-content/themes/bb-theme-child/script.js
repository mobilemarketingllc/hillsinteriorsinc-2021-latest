(function($) {
    $(document).on('facetwp-loaded', function() {


        refreshSlickSlider();

        function refreshSlickSlider(){

        
        $('.color_variations_slider > .slides').slick({
            dots: false,
            infinite: false,
            speed: 300,
            arrows: true,
            slidesToShow: 6,
            slidesToScroll: 6,
            mobileFirst: false,
            prevArrow: '<a href="javascript:void(0)" class="arrow slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>',
            nextArrow: '<a href="javascript:void(0)" class="arrow slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>',
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 6
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    }
     });
})(jQuery);